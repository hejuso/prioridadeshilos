public class Animal extends Thread {

	public static void main(String[] args) throws InterruptedException {

		// Creamos los objetos tipo animal
		Animal guepardo = new Animal("Guepardo");
		Animal liebre = new Animal("Liebre");
		Animal tortuga = new Animal("Tortuga");

		// Asignamos las prioridades
		guepardo.setPriority(Thread.MAX_PRIORITY);
		liebre.setPriority(Thread.NORM_PRIORITY);
		tortuga.setPriority(Thread.MIN_PRIORITY);

		// Ejecutamos los hilos
		guepardo.start();
		liebre.start();
		tortuga.start();

	}

	// Constructor del objeto
	public Animal(String str) {
		super(str);
	}

	public void run() {
		// Asignamos la prioridad a los hilos
		
		System.out.println("Prioridad "+Thread.currentThread().getPriority()+" para "+Thread.currentThread().getName());

		System.out.println("El animal " + Thread.currentThread().getName() + " ha llegado a la meta");
		yield();

		
	}

	// Aunque creo que el planteamiento de la actividad est�� correcto, no se est��n
	// ejecutando correctamente las prioridades. Por lo que he podido leer en
	// Stackoverflow, parece que las prioridades no siempre garantizan el orden en
	// el que va a salir y dependen del tipo de sistema operativo, CPU, etc.
	// Eso, o que habr�� cometido alg��n error garrafal, que tambi��n es muy probable :)
	
	// Link de referencia:
	// https://stackoverflow.com/questions/12038592/java-thread-priority-has-no-effect?answertab=votes#tab-top

}